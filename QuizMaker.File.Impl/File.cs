﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using QuizMaker.File.Contract;
using System.IO;

namespace QuizMaker.File.Impl
{
    [Serializable]
    class Data : IData
    {
        [XmlElement("Author")]
        public string Author { get; set; }
        [XmlElement("Date")]
        public int Date { get; set; }
        [XmlElement("Era")]
        public int Era { get; set; }
        [XmlElement("Name")]
        public string Name { get; set; }
        [XmlElement("Style")]
        public string Style { get; set; }
        [XmlElement("WhereMade")]
        public string WhereMade { get; set; }
        [XmlElement("WhereNow")]
        public string WhereNow { get; set; }
        [XmlElement("Technique")]
        public string Technique { get; set; }


        public Data() { }
        public Data(string Author, int Date, int Era, string Name, string Style, string WhereMade, string WhereNow, string Technique)
        {
            this.Author = Author;
            this.Date = Date;
            this.Era = Era;
            this.Name = Name;
            this.Style = Style;
            this.WhereMade = WhereMade;
            this.WhereNow = WhereNow;
            this.Technique = Technique;
        }

        internal static bool Exists(string p)
        {
            throw new NotImplementedException();
        }
    }
    public class File : IFile
    {
        IList<Data> file;

        public void XMLSerialization(string Author, int Date, int Era, string Name, string Style, string WhereMade, string WhereNow, string Technique)
        {
            //if (XMLDeSerialization().Count == 0)
            file = new List<Data>();
            //else
            //    Serialization = XMLDeSerialization();

            file.Add(new Data(Author, Date, Era, Name, Style, WhereMade, WhereNow, Technique));

            XmlSerializer xmlser = new XmlSerializer(typeof(List<Data>));
            StreamWriter sw = null;
            try
            {
                sw = new StreamWriter("dane.xml");
                xmlser.Serialize(sw, file);
            }
            catch
            {
                throw new Exception();
            }
            finally
            {
                if (null != sw)
                {
                    sw.Dispose();
                }
            }
        }

        public List<Data> XMLDeSerialization()
        {
            List<Data> Serialization1;

            IData file;
            file = new Data();
            Serialization1 = new List<Data>();

            if (Data.Exists(Path.Combine(".//", "dane.xml")))
            {
                StreamReader sr = new StreamReader("dane.xml");
                XmlSerializer xmlser = new XmlSerializer(typeof(List<Data>));
                Serialization1 = (List<Data>)xmlser.Deserialize(sr);
                sr.Dispose();
            }
            return Serialization1;
        }

    }
}

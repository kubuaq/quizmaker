﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace QuizMaker.GUI.Files
{
    /// <summary>
    /// Interaction logic for Menu.xaml
    /// </summary>
    public partial class Menu : Window
    {
        public Menu()
        {
            InitializeComponent();
        }

        private void Add_Click(object sender, RoutedEventArgs e)
        {
            Add AddFiles = new Add();

            AddFiles.Left = this.Left;
            AddFiles.Top = this.Top;
            AddFiles.Show();
            this.Close();
        }
        private void Edit_Click(object sender, RoutedEventArgs e)
        {
            Edit EditFiles = new Edit();

            EditFiles.Left = this.Left;
            EditFiles.Top = this.Top;
            EditFiles.Show();
            this.Close();
        }
        private void Delete_Click(object sender, RoutedEventArgs e)
        {
            Delete DeleteFiles = new Delete();

            DeleteFiles.Left = this.Left;
            DeleteFiles.Top = this.Top;
            DeleteFiles.Show();
            this.Close();
        }
    }
}

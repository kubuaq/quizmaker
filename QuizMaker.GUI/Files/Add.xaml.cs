﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using QuizMaker.File.Contract;
using QuizMaker.File.Impl;

namespace QuizMaker.GUI.Files
{
    /// <summary>
    /// Interaction logic for Add.xaml
    /// </summary>
    public partial class Add : Window
    {
        public Add()
        {
            InitializeComponent();
        }
        private void AddFiles_Button_Click(object sender, RoutedEventArgs e)
        {

            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();


            dlg.DefaultExt = ".txt";
            dlg.Filter = "All Files (*.*)|*.*|JPEG Files (*.jpeg)|*.jpeg|PNG Files (*.png)|*.png|JPG Files (*.jpg)|*.jpg|GIF Files (*.gif)|*.gif";


            Nullable<bool> result = dlg.ShowDialog();


            if (result == true)
            {
                BitmapImage logo = new BitmapImage();
                logo.BeginInit();
                logo.UriSource = new Uri(dlg.FileName);
                logo.EndInit();

                AddFiles_Image_Image.Source = logo;
            }
        }

        /// <summary>
        /// przycisk resetujący odpowiedzi
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddFiles_Button_Reset_Click(object sender, RoutedEventArgs e)
        {
            AddFiles_TextBox_Author.Text = "";
            AddFiles_TextBox_Date.Text = "";
            AddFiles_TextBox_Era.Text = "";
            AddFiles_TextBox_Name.Text = "";
            AddFiles_TextBox_Style.Text = "";
            AddFiles_TextBox_WhereMade.Text = "";
            AddFiles_TextBox_WhereNow.Text = "";
            AddFiles_TextBox_Technique.Text = "";
        }


        /// <summary>
        /// przycisk wysyłający formularz
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void AddFiles_Button_Add_Click(object sender, RoutedEventArgs e)
        {
            //AddFiles_TextBox_Author.Text = Author;
            //AddFiles_TextBox_Date.Text = Date;
            //AddFiles_TextBox_Era.Text = Era;
            //AddFiles_TextBox_Name.Text = Name;
            //AddFiles_TextBox_Style.Text = Style;
            //AddFiles_TextBox_WhereMade.Text = WhereMade;
            //AddFiles_TextBox_WhereNow.Text = WhereNow;
            //AddFiles_TextBox_Technique.Text = Technique;
            

        }
    }
}

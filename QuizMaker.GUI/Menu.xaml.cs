﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;

namespace QuizMaker.GUI
{
    public partial class Menu : Window
    {
        public Menu()
        {
            InitializeComponent();
        }
        private void Quiz_Click(object sender, RoutedEventArgs e)
        {
            Quiz Quiz = new Quiz();

            Quiz.Left = this.Left;
            Quiz.Top = this.Top;
            Quiz.Show();
            this.Close();
        }
        private void Opcje_Click(object sender, RoutedEventArgs e)
        {
            Options Options = new Options();

            Options.Left = this.Left;
            Options.Top = this.Top;
            Options.Show();
            this.Close();
        }
        private void Pytania_Click(object sender, RoutedEventArgs e)
        {
            Files.Menu FilesMenu = new Files.Menu();

            FilesMenu.Left = this.Left;
            FilesMenu.Top = this.Top;
            FilesMenu.Show();
            this.Close();
        }

        private void Wyniki_Click(object sender, RoutedEventArgs e)
        {
            LadderBoard LadderBoard = new LadderBoard();

            LadderBoard.Left = this.Left;
            LadderBoard.Top = this.Top;
            LadderBoard.Show();
            this.Close();
        }

        private void Quit_Click(object sender, RoutedEventArgs e)
        {
            Application.Current.Shutdown();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace QuizMaker.File.Contract
{
    public interface IData {  }
    public interface IFile
    {
        void XMLSerialization(string Author, int Date, int Era, string Name, string Style, string WhereMade, string WhereNow, string Technique);
    }
}
